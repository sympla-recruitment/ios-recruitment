# Recrutamento iOS

Primeiramente, muito obrigado por escolher a Sympla! 🙂

Fazer parte da Sympla envolve ser parte essencial da nossa cultura e do nosso negócio. Quando pensamos em nosso time de desenvolvimento, fazer parte da Sympla também significa estar alinhado com nossas convicções sobre os alicerces de um bom processo de desenvolvimento. Nada melhor para mostrar do que um desenvolvedor é feito do que escrever código 🤘

## Sobre o exercício

O objetivo desta etapa do processo é conhecer seu estilo de codificação em Swift e sua abordagem para arquitetar, organizar e testar um app iOS escrito do zero. Para isso, você é livre para escolher entre duas opções:

**1.** Uma app para conversão de moedas

**2.** Um browser para a API de usuários do GitHub

Ambas são equivalentes sob a ótica de avaliarmos seu nível e seu conhecimento enquanto desenvolvedor iOS.

### Conversor de moedas

A primeira opção é desenvolver um app que permita a conversão entre duas moedas, como por exemplo entre o dólar e o real ou entre o euro e o peso argentino. O problema da experiência de usuário para um app como esse pode ser solucionado de inúmeras formas diferentes e você tem carta branca para abordar a solução da forma como preferir.

Você também é livre para usar a fonte de dados que preferir para consultar informações sobre o valor de cada moeda (naturalmente em relação a uma moeda base). Como sugestão, a API [https://exchangeratesapi.io/](https://exchangeratesapi.io/) é totalmente grátis e versátil, permitindo que você filtre a lista de moedas e escolha uma moeda base arbitrária para servir de referência para o cálculo dos valores unitários.

### Browser de usuários do GitHub

A segunda opção é desenvolver um app que permita usar uma pesquisa de texto livre para encontrar usuários do GitHub. Além de encontrar usuários, o app deve conseguir listar os respositórios de um usuário e também deve conseguir visualizar detalhes sobre o perfil público do usuário selecionado.

Assim como na primeira opção, a experiência de usuário para um app como esse pode ser abordada de muitas formas diferentes e deixamos para você a responsabilidade de decidir como apresentar as informações que pedimos da melhor maneira possível. No CocoaHeads Conference de 2016, Guilherme Rambo usou como pano de fundo para sua palestra um app que ele desenvolveu chamado [Astronomer](https://github.com/insidegui/Astronomer) que serviu de inspiração para escolhermos este tema para o exercício. Caso queira, você pode se basear completamente nesta UI.

## Objetivos

O nosso objetivo com este problema prático é conhecer você como profissional e, acima de tudo, como programador. Por isso, não vamos estabelecer nenhum tipo de restrição sobre padrões de projeto, gerenciamento de dependências, construção de UI ou boas práticas. Queremos que você esteja à vontade para programar da maneira que mais gosta, e que você tenha autonomia para construir o projeto do seu jeito, com as suas regras e imprimindo o seu estilo.

Só temos um pedido: encare este projeto como algo que você encontraria em seu dia-a-dia profissional, ou como um projeto de estimação. Em outras palavras, queremos que você sinta prazer programando e que você se dedique ao projeto como se fosse seu. O resto é por sua conta 😬

## Submissão

Vamos flexibilizar o processo de submissão em duas opções:

1. Criar um [fork deste repositório](https://gitlab.com/sympla-recruitment/ios-recruitment/forks/new), que é **público**, e fazer um [merge request](https://gitlab.com/sympla-recruitment/ios-recruitment/merge_requests/new) ao final do desenvolvimento. Escolhendo esta opção, seu fork também será público.

2. Criar um repositório privado com o nome `ios-recruitment` em seu host de projetos favorito e entrar em contato através do e-mail [thamiris.meireles@sympla.com.br](mailto:thamiris.meireles@sympla.com.br) para prosseguirmos com o acesso ao código.

## Dicas

Se você chegou até esta etapa, significa que queremos que você dê o seu melhor e que tenha muito sucesso. Temos algumas dicas sobre o que esperamos da sua solução:

1. Escolha bons nomes para as variáveis, classes e métodos. Um bom nome atinge o equilíbrio entre ser descritivo e ser conciso. Não é uma tarefa fácil, mas focar nisso vale a pena.

2. Nós escrevemos muitos testes aqui na Sympla. Não acreditamos em porcentagens de cobertura, mas achamos que o que pode e vale a pena ser testado deve ser testado. Não deixe de incluir testes para o que faz sentido na sua solução.

3. Nossa base de código faz uso extenso de programação reativa usando RxSwift. Pense onde e por quê você usaria RxSwift ou ideias equivalentes na sua solução.

4. O fluxo de dados do app da Sympla é unidirecional totalmente baseado em Redux (https://redux.js.org). Pense se um fluxo de dados baseado em Redux faz sentido para a sua solução.

5. Considere utilizar view code para desenhar a UI da sua solução. Toda a nossa UI é desenhada utilizando view code, e temos interesse em avaliar sua experiência nesse quesito.

6. A clareza e a legibilidade do seu código são muito mais importantes que a engenharia da sua solução. O princípio DRY é importante, mas ele nunca será mais importante que a legibilidade.

## Contato

Buscamos o máximo de clareza possível ao documentar o exercício, e esperamos que todas as dúvidas possam ser solucionadas com este `README`. Mas as dúvidas virão, e estamos prontos! Fique à vontade para entrar em contato conosco através do endereço [thamiris.meireles@sympla.com.br](mailto:thamiris.meireles@sympla.com.br) para tirar dúvidas e pedir ajuda. Pedimos apenas que as dúvidas sejam pertinentes ao exercício, para que possamos dar toda a atenção necessária a todos os candidatos.
